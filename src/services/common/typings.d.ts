/*
 * @Description:
 * @Author: mengtao
 * @Date: 2021-12-22 09:57:32
 * @LastEditTime: 2022-01-20 14:14:27
 */
// @ts-ignore
/* eslint-disable */

declare namespace COMMON_API {

  type UserInfo =  {
    id: number;
    user_no: string;
    phone: string;
    avatar: string;
    username: string;
    email: string;
    gender: number;
    type: number;
    state: number;
    t_roles: MENU_API.Role[];
    created_at: string;
    updated_at: string;
    delete_at?: string;
  };

  type Settings = {
    id: number;
    title: string;
    title_en: string;
    logo: string;
    version: string;
    slogan: string;
    iconfontProjectUrl: string;
    iconfontUrl: string;
    created_at: string;
    updated_at: string;
  }

  type SettingsResult = {
    data: Settings;
    code: number;
    msg: string;
  }

  type CurrentUser = { userInfo: UserInfo}  & {settings: Settings};

  type LoginParams = {
    user_no?: string;
    password?: string;
    captchaCode?: string;
    email?: string;
    type: number;
    uuid?: string;
  };

  type CommonResult = {
    data: any;
    code: number;
    msg: string;
  };

  type CaptchaResult = {
    data: {img: string, uuid: string};
    code: number;
    msg: string;
  }

  type LoginResult = {
    data: string;
    code: number;
    msg: string;
    token: string;
  };

  type PageParams = {
    current?: number;
    pageSize?: number;
  };

  type Menu = {
    id?: string;
    name?: string;
    name_cn?: string;
    path?: string;
    parent_id?: string;
    kind?: number;
    icon?: string;
    title?: string;
    hidden?: number;
    status?: number;
    routes?: Menu[];
    access?: string;
    component?: string;
    location?: string;
    hideInMenu?: boolean;
    perms?: string;
    sort?: number;
    created_at?: string;
    updated_at?: string;
  };

  type Routers = {
    allMenuList?: Menu[];
    menuList?: Menu[];
    hasRoute?: string[];
    permsList?: Array<string>;
  };

  type Columns = {
    title: string;
    dataIndex: string;
    align?: string;
    width?: number | string;
    className?: string;
    key?: string;
    render?: (text?: any, record?: T) => any;
  };
}
