import { request } from 'umi';

export async function login(body: COMMON_API.LoginParams, options?: Record<string, any>) {
  return request<COMMON_API.LoginResult>('/api/v1/admin_users/login', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

export async function getInfo() {
  return request<{
    data: COMMON_API.CurrentUser;
  }>('/api/v1/admin_users/getInfo', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

export async function getRouters() {
  return request<{
    data: COMMON_API.Routers;
  }>('/api/v1/getRouters', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

export async function logout() {
  return request('/api/v1/admin_users/logout', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

/** 邮箱发送验证码 */
export async function getEmailCaptcha(params: {
  // query
  /** 手机号 */
  email?: string;
}) {
  return request<COMMON_API.CommonResult>('/api/v1/sendMailer', {
    method: 'GET',
    params: {
      ...params,
    },
  });
}

/** 登录验证信息 */
export async function getCaptcha() {
  return request<COMMON_API.CaptchaResult>('/api/v1/getCaptchaCode', {
    method: 'GET',
  });
}

/** 刷新token */
export async function refreshToken() {
  return request<COMMON_API.CommonResult>('/api/v1/refreshToken', {
    method: 'PUT',
  });
}

/** 获取系统配置信息 */
export async function getSettingsInfo() {
  return request<COMMON_API.SettingsResult>('/api/v1/settings', {
    method: 'GET',
  });
}

/** 修改系统配置信息 */
export async function updateSettingsInfo(data: COMMON_API.Settings) {
  return request<COMMON_API.CommonResult>('/api/v1/settings', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data
  });
}

/** 上传文件 */
export async function uploadFile(params: { single: number, category: string}, data: any) {
  return request<COMMON_API.CommonResult>('/api/v1/file', {
    method: 'POST',
    requestType: 'form',
    params: {
      ...params,
    },
    data,
  });
}

