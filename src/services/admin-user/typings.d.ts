/*
 * @Description:
 * @Author: mengtao
 * @Date: 2021-12-28 14:14:34
 * @LastEditTime: 2022-01-07 16:31:26
 */

declare namespace ADMIN_USER_API {
  type AdminUser = {
    id: number;
    user_no: string;
    phone: string;
    avatar: string;
    username: string;
    email: string;
    gender: number;
    type: number;
    state: number;
    created_at: string;
    updated_at: string;
    deleted_at?: string;
    t_roles: {role_name: string, is_default: number, id: number}[];
  };

  type AddAdminUser = {
    phone: string;
    avatar?: string;
    username: string;
    email: string;
    gender: number;
    type: number;
    state: number;
  };

  type UpdateAdminUser = AddAdminUser & { id: number } & { roles: number[] };

  type AdminUserResponse = {
    code?: number;
    data?: {
      total?: number;
      list?: AdminUser[];
      pageNo?: number;
      pageSize?: number;
    };
    msg?: string;
  };

  type AdminUserRequest = {
    user_no?: string;
    phone?: string;
    username?: string;
    email?: string;
    gender?: number;
    state?: number;
    pageNo?: number;
    pageSize?: number;
  };

  type UpdateAdminUserState = {
    id: number;
    state: number;
  };
}
