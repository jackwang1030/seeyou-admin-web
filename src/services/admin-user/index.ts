/*
 * @Description:
 * @Author: mengtao
 * @Date: 2021-12-28 14:07:32
 * @LastEditTime: 2021-12-30 10:23:35
 */

import { request } from 'umi';

/** 获取系统用户列表 */
export async function getAdminUserList(params: ADMIN_USER_API.AdminUserRequest) {
  return request<ADMIN_USER_API.AdminUserResponse>('/api/v1/admin_users', {
    method: 'GET',
    params: {
      ...params,
    },
  });
}

/** 添加系统用户 */
export async function addAdminUser(
  body: ADMIN_USER_API.AddAdminUser,
  options?: Record<string, any>,
) {
  return request<COMMON_API.CommonResult>('/api/v1/admin_users', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 修改系统用户 */
export async function updateAdminUser(
  body: ADMIN_USER_API.UpdateAdminUser,
  options?: Record<string, any>,
) {
  return request<COMMON_API.CommonResult>('/api/v1/admin_users', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 修改系统用户状态 */
export async function updateAdminUserState(
  body: ADMIN_USER_API.UpdateAdminUserState,
  options?: Record<string, any>,
) {
  return request<COMMON_API.CommonResult>('/api/v1/admin_users/updateState', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/** 删除用户 */
export async function deleteAdminUser(body: number[], options?: Record<string, any>) {
  return request<COMMON_API.CommonResult>('/api/v1/admin_users', {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
