import { request } from 'umi';

/**
 * @description: 获取资源列表(所有的)
 * @param {MENU_API} params
 * @return {*}
 */
export async function getResourcesList(params: MENU_API.MenuRequest) {
  return request<MENU_API.MenuResponse>('/api/v1/getAllResources', {
    method: 'GET',
    params: {
      ...params,
    },
  });
}

/**
 * @description: 获取资源tree
 * @return {*}
 */
export async function getTreeResourceList(params: { kindIds?: string }) {
  return request<MENU_API.MenuTreeResponse>('/api/v1/getTreeResources', {
    method: 'GET',
    params: {
      ...params,
    },
  });
}

/**
 * @description: 新增资源
 * @param {MENU_API} body
 * @param {Record} options
 * @param {*} any
 * @return {*}
 */
export async function addResource(body: MENU_API.MenuAdd, options?: Record<string, any>) {
  return request<COMMON_API.CommonResult>('/api/v1/resources', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/**
 * @description: 修改资源
 * @param {MENU_API} body
 * @param {Record} options
 * @param {*} any
 * @return {*}
 */
export async function updateResource(body: MENU_API.MenuUpdate, options?: Record<string, any>) {
  return request<COMMON_API.CommonResult>('/api/v1/resources', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/**
 * @description: 删除资源
 * @param {string} body
 * @param {Record} options
 * @param {*} any
 * @return {*}
 */
export async function deleteResource(body: { id: string }, options?: Record<string, any>) {
  return request<COMMON_API.CommonResult>('/api/v1/resources', {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
