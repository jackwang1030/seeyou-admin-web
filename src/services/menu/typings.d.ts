declare namespace MENU_API {
  type Menu = {
    id: string;
    name: string;
    name_cn: string;
    path?: string;
    title?: string;
    value?: string;
    parent_id: string;
    kind: number;
    icon?: string;
    hidden: number;
    status: number;
    component?: string;
    location?: string;
    perms: string;
    access: string;
    sort?: number;
    remark?: string;
    children?: Menu[];
    created_at: string;
    updated_at: string;
  };

  type MenuAdd = {
    name: string;
    name_cn: string;
    path?: string;
    parent_id: string;
    kind: number;
    icon?: string;
    hidden: number;
    status: number;
    component?: string;
    location?: string;
    perms?: string;
    access?: string;
    sort?: number;
    remark?: string;
  };

  type Resources = {
    id: string;
    key: string;
    kind: number;
    parent_id: string;
    title: string;
  };

  type MenuUpdate = MenuAdd & { id: string };

  type MenuResponse = {
    code: number;
    data: Menu[] | [];
    msg: string;
  };

  type MenuRequest = {
    name_cn?: string;
    status?: number;
  };

  type MenuTreeResponse = {
    code: number;
    data: {
      resourceIdList: string[];
      resourceTree: MENU_API.Resources[];
    };
    msg: string;
  };
}
