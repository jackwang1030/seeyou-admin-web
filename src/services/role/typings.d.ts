declare namespace ROLE_API {
  type Role = {
    id: number;
    role_name: string;
    is_default: number;
    remark?: any;
    created_at: string;
    updated_at: string;
  };

  type RoleResponse = {
    code: number;
    data: {
      list: Role[];
      total?: number;
      current?: number;
      pageSize?: number;
    };
    msg?: string;
  };

  type AddRole = {
    role_name: string;
    is_default: number;
    resourceIds: string;
    remark?: any;
  };

  type RoleInfoResponse = {
    code: number;
    data: {
      role: Role;
      checkList: string[];
      resourceList: MENU_API.Resources[];
      resourceIdList: string[];
      resourceTree?: MENU_API.Resources[];
    };
    msg: string;
  };

  type UpdateRole = AddRole & { id: number };
}
