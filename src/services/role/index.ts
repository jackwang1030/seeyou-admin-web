import { request } from 'umi';

/**
 * @description: 获取角色列表
 * @param {object} params
 * @return {*}
 */
export async function getRoleList(params: {
  role_name?: string;
  current?: number;
  pageSize?: number;
}) {
  return request<ROLE_API.RoleResponse>('/api/v1/roles', {
    method: 'GET',
    params: {
      ...params,
    },
  });
}

/**
 * @description: 获取角色详情
 * @param {object} params
 * @return {*}
 */
export async function getRoleInfo(params: { id: number }) {
  return request<ROLE_API.RoleInfoResponse>('/api/v1/roles/getInfo', {
    method: 'GET',
    params: {
      ...params,
    },
  });
}

/**
 * @description: 新增角色
 * @param {ROLE_API} body
 * @param {Record} options
 * @param {*} any
 * @return {*}
 */
export async function addRole(body: ROLE_API.AddRole, options?: Record<string, any>) {
  return request<COMMON_API.CommonResult>('/api/v1/roles', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/**
 * @description: 修改角色
 * @param {ROLE_API} body
 * @param {Record} options
 * @param {*} any
 * @return {*}
 */
export async function updateRole(body: ROLE_API.UpdateRole, options?: Record<string, any>) {
  return request<COMMON_API.CommonResult>('/api/v1/roles', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/**
 * @description: 删除角色
 * @param {string} body
 * @param {Record} options
 * @param {*} any
 * @return {*}
 */
export async function deleteRole(body: { ids: number[] }, options?: Record<string, any>) {
  return request<COMMON_API.CommonResult>('/api/v1/roles', {
    method: 'DELETE',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}

/**
 * @description: 修改角色为默认角色
 * @param {object} body
 * @param {Record} options
 * @param {*} any
 * @return {*}
 */
export async function changeRoleIsDefault(body: { id: number }, options?: Record<string, any>) {
  return request<COMMON_API.CommonResult>('/api/v1/roles/is_default', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    data: body,
    ...(options || {}),
  });
}
