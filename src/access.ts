/*
 * @Description:
 * @Author: mengtao
 * @Date: 2021-12-19 22:43:41
 * @LastEditTime: 2021-12-26 21:58:25
 */
/**
 * @see https://umijs.org/zh-CN/plugins/plugin-access
 * */
export default function access(initialState: { hasRoute?: string[] | undefined }) {
  const { hasRoute } = initialState || {};
  return {
    permission: (route: COMMON_API.Menu) => {
      return hasRoute && hasRoute.includes(route.id as string);
    },
  };
}
