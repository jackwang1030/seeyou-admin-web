import { useModel } from 'umi';
import { isPermission } from '@/utils/utils';

function usePermission(permission: string): boolean {
  const { initialState } = useModel('@@initialState');
  return isPermission(permission, initialState?.permsList || []);
}

export default usePermission;
