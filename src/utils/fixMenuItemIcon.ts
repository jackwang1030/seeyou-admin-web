import React from 'react';
import * as allIcons from '@ant-design/icons';


// FIX从接口获取菜单时icon为string类型
const fixMenuItemIcon = (menus: COMMON_API.Menu[], iconType = 'Outlined'): COMMON_API.Menu[] => {
  menus.forEach((item) => {
    const { icon, routes } = item;
    if (typeof icon === 'string' && icon) {
      const fixIconName = icon.slice(0, 1).toLocaleUpperCase() + icon.slice(1) + iconType;
      item.icon = React.createElement(allIcons[fixIconName] || allIcons[icon]);
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-expressions
    // routes && routes.length > 0 ? (item.routes = fixMenuItemIcon(routes)) : null;
  });
  return menus;
};


export { fixMenuItemIcon };
