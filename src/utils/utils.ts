import JsEncrypt from 'jsencrypt/bin/jsencrypt.min';


const publicKey: string = `
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDo9td2/8xmBb82k57wKRVf+pLn
1/PfQKUWGu5jmEkGfb9Vxt2wTNBKh3f2pzKCyeIml0XU5ifA6bq8Dvk28VBuKzyB
5DgIYrF7o5MCABWX8LjFtIQjElAI8cRUYxF/6Fqu99bmjQ/KIKc8GzpF9bS1ZQJt
onZsoc3KUnPP7QBUaQIDAQAB
-----END PUBLIC KEY-----
`;

export function isPermission(permission: string, permsList: string[]): boolean {
  let isPerm: boolean = false;
  if (permsList.length > 0) {
    isPerm = permsList.includes(permission);
  }
  return isPerm;
}

// 密码加密
export const RSAencrypt = (text: string, key = publicKey): string => {
  const jse = new JsEncrypt();
  //设置公钥
  jse.setPublicKey(key);
  return jse.encrypt(text) as string;
};

// 根据key获取数组value
export const getArrayProps = (arr: any[], key: string) => {
  const keys = `${key}`;
  const res: string[] = [];
  if (arr.length > 0) {
    arr.forEach((t) => {
      res.push(t[keys]);
    });
  }
  return res;
};


