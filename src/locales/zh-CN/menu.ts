/*
 * @Description:
 * @Author: mengtao
 * @Date: 2021-12-19 22:43:41
 * @LastEditTime: 2021-12-29 09:38:45
 */
export default {
  'menu.welcome': '欢迎',
  'menu.home': '首页',
  'menu.admin': '管理页',
  'menu.login': '登录',
  'menu.register': '注册',
  'menu.dashboard': 'Dashboard',
  'menu.dashboard.analysis': '分析页',
  'menu.dashboard.monitor': '监控页',
  'menu.dashboard.workplace': '工作台',
  'menu.exception.403': '403',
  'menu.exception.404': '404',
  'menu.exception.500': '500',
  'menu.result': '结果页',
  'menu.result.success': '成功页',
  'menu.result.fail': '失败页',
  'menu.exception': '异常页',
  'menu.exception.not-permission': '403',
  'menu.exception.not-find': '404',
  'menu.exception.server-error': '500',
  'menu.account': '个人页',
  'menu.account.center': '个人中心',
  'menu.account.settings': '个人设置',
  'menu.account.trigger': '触发报错',
  'menu.account.logout': '退出登录',
  'menu.systemManage': '系统管理',
  'menu.systemManage.userManage': '系统用户',
  'menu.systemManage.roleManage': '角色管理',
  'menu.systemManage.menuManage': '菜单管理',
  'menu.contentManage': '内容管理',
  'menu.contentManage.contentCheckManage': '内容审核',
};
