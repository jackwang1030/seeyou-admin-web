﻿export default [
  {
    id: '0c1477e1-f363-446c-8186-1637511c995f',
    name: '首页',
    path: '/welcome',
    access: 'permission',
    component: './Welcome',
    parent_id: '0',
  },
  {
    id: 'ffddb99f-a206-41f0-bf8c-c2ab86ee20f7',
    name: '系统管理',
    path: '/system',
    access: 'permission',
    parent_id: '0',
    routes: [
      {
        id: '4e4f2873-7ecc-4ea5-aca7-6ea530126fb0',
        name: '系统用户',
        path: '/system/adminUser',
        access: 'permission',
        component: './System/AdminUser',
        parent_id: 'ffddb99f-a206-41f0-bf8c-c2ab86ee20f7',
      },
      {
        id: '61545680-ba70-4116-b6ed-91a227153979',
        name: '角色管理',
        path: '/system/role',
        access: 'permission',
        component: './System/Role',
        parent_id: 'ffddb99f-a206-41f0-bf8c-c2ab86ee20f7',
      },
      {
        id: 'd369024f-41d3-4678-a81c-961b0626ac97',
        name: '菜单管理',
        path: '/system/menu',
        access: 'permission',
        component: './System/Menu',
        parent_id: 'ffddb99f-a206-41f0-bf8c-c2ab86ee20f7',
      },
      {
        id: '60d884e1-eb90-43ea-a1c2-c8ee0f79befa',
        name: '系统设置',
        path: '/system/sysSettings',
        access: 'permission',
        component: './System/SysSettings',
        parent_id: 'ffddb99f-a206-41f0-bf8c-c2ab86ee20f7',
      },
    ],
  },
  {
    id: 'd8688d33-6d55-4ed6-8c59-987c716d9d22',
    name: '内容管理',
    path: '/content',
    access: 'permission',
    parent_id: '0',
    routes: [
      {
        id: '9acf7262-aae4-4058-a127-73ecc3bc8acc',
        name: '内容审核',
        path: '/content/check',
        access: 'permission',
        component: './Content/Check',
        parent_id: 'd8688d33-6d55-4ed6-8c59-987c716d9d22',
      },
    ],
  },
  {
    path: '/user',
    layout: false,
    routes: [
      {
        path: '/user',
        routes: [
          {
            name: 'login',
            path: '/user/login',
            component: './user/Login',
          },
        ],
      },
      {
        component: './404',
      },
    ],
  },
  {
    path: '/',
    redirect: '/welcome',
  },
  {
    component: './404',
  },
];
